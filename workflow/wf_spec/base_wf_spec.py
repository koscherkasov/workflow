from sqlalchemy.orm.exc import NoResultFound

from workflow.model.wf_model import WfTask, Workflow
from workflow.wf_state import WfConstants


class BaseWfSpec(object):
    def __init__(self):
        self.start_wf_task = None
        self.type_tasks_dict = dict()
        self.service_tasks = []
        self.all_tasks = []

    def create_wf(self, list_task_information, name, factory_name, session, object_code=None):
        self.start_wf_task = WfTask(identification_name='Start wf_task {}'.format(name),
                                    service_type=WfConstants.START,
                                    state=WfConstants.READY)
        self.fill_type_tasks_dict(list_task_information, factory_name, session)
        self.create_structure()
        self.fill_all_tasks()
        wf_id = self.add_all_to_db(name, factory_name, session, object_code)
        return wf_id

    def fill_type_tasks_dict(self, list_task_information, factory_name, session):
        """
            :param list_task_information: dict with keys: type, sub_type, identification_name, associate_with_wf_task_id;
            :type list_task_information: dict;
            :param factory_name: factory name;
            :type factory_nameL str;
            :param session: SQLAlchemy session.
        """
        for task_info in list_task_information:
            wf_task = None
            associated_wf_task_id = task_info.associate_with_wf_task_id
            if associated_wf_task_id:
                try:
                    wf_task = session.query(WfTask).filter_by(id=associated_wf_task_id).one()
                    for wf in wf_task.wfs:
                        if wf.factory_name != factory_name:
                            raise AnotherFactoryTaskAssociation(
                                'Attempt to associate task in workflow with factory={} '
                                'with task from another factory={}'.format(factory_name, wf.factory_name)
                            )
                except NoResultFound:
                    raise AssociatedWfTaskNotExist(
                        'Attempt to associate with not existed task with id={}'.format(associated_wf_task_id))
            task_type = task_info.type
            task_list = self.type_tasks_dict.get(task_type)
            if not task_list:
                self.type_tasks_dict[task_type] = []
            if wf_task:
                self.type_tasks_dict[task_type].append(wf_task)
                continue
            if task_type == WfConstants.SUBWF:
                try:
                    wf = session.query(Workflow).filter_by(id=task_info.subwf_id).one()
                    if wf.factory_name != factory_name:
                        raise AnotherFactoryTaskSubWf(
                            'Attempt to add subwf with factory={} in workflow '
                            'with factory={}'.format(wf.factory_name, factory_name)
                        )
                    self.type_tasks_dict[task_type].append(WfTask(type=WfConstants.SUBWF,
                                                                  sub_type=task_info.sub_type,
                                                                  identification_name=task_info.identification_name,
                                                                  service_type=WfConstants.SUBWF,
                                                                  subwf_id=task_info.subwf_id,
                                                                  state=WfConstants.FUTURE))
                except NoResultFound:
                    raise SubWfNotExist(
                        'Attempt to add subwf task with not existed wf with id={}'.format(task_info.subwf_id))

                continue
            if task_type == WfConstants.CHOICE:
                self.type_tasks_dict[task_type].append(WfTask(type=WfConstants.CHOICE,
                                                              sub_type=task_info.sub_type,
                                                              identification_name=task_info.identification_name,
                                                              service_type=WfConstants.CHOICE,
                                                              state=WfConstants.FUTURE))
                continue
            self.type_tasks_dict[task_type].append(WfTask(type=task_info.type,
                                                          sub_type=task_info.sub_type,
                                                          identification_name=task_info.identification_name,
                                                          state=WfConstants.FUTURE))

    def create_structure(self):
        raise NotImplementedError()

    def fill_all_tasks(self):
        self.all_tasks.append(self.start_wf_task)
        for type, list_task in self.type_tasks_dict.items():
            for wf_task in list_task:
                self.all_tasks.append(wf_task)
        for service_task in self.service_tasks:
            self.all_tasks.append(service_task)

    def add_all_to_db(self, name, factory_name, session, object_code=None):
        wf = Workflow(name=name, first_wf_task=self.start_wf_task, factory_name=factory_name, object_code=object_code)
        wf.add_tasks(self.all_tasks)
        session.add(self.start_wf_task)
        for task in self.all_tasks:
            session.add(task)
        session.add(wf)
        session.commit()
        return wf.id

    def create_join_task(self, name):
        join_task = WfTask(identification_name=name,
                           service_type=WfConstants.JOIN,
                           state=WfConstants.FUTURE)
        self.service_tasks.append(join_task)
        return join_task

    def create_subwf_task(self, name, subwf_id):
        subfw_task = WfTask(identification_name=name,
                            service_type=WfConstants.SUBWF,
                            subwf_id=subwf_id,
                            state=WfConstants.FUTURE)
        self.service_tasks.append(subfw_task)
        return subfw_task


class AssociatedWfTaskNotExist(Exception):
    pass


class AnotherFactoryTaskAssociation(Exception):
    pass


class AnotherFactoryTaskSubWf(Exception):
    pass


class SubWfNotExist(Exception):
    pass
