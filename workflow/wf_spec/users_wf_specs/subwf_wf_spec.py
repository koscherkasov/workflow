from workflow.wf_spec.base_wf_spec import BaseWfSpec
from workflow.wf_state import WfConstants


class InnerWfSpec(BaseWfSpec):
    """
    self.type_tasks_dict is dict with key = str type and value = list of instances Task
    """

    def create_structure(self):
        first_task = self.type_tasks_dict['first'][0]
        second_task = self.type_tasks_dict['should_approve_type'][0]
        self.start_wf_task.connect(first_task)
        first_task.connect(second_task)


class OuterWfSpec(BaseWfSpec):
    """
    self.type_tasks_dict is dict with key = str type and value = list of instances Task
    """

    def create_structure(self):
        first_task = self.type_tasks_dict['first'][0]
        second_task = self.type_tasks_dict['should_approve_type'][0]
        subwf_task = self.type_tasks_dict[WfConstants.SUBWF][0]
        third_task = self.type_tasks_dict['third'][0]
        fourth_task = self.type_tasks_dict['fourth'][0]
        self.start_wf_task.connect(first_task)
        first_task.connect(second_task)
        second_task.connect(subwf_task)
        subwf_task.connect(third_task)
        third_task.connect(fourth_task)
