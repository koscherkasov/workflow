from workflow.wf_spec.base_wf_spec import BaseWfSpec
from workflow.wf_state import WfConstants


class BigTest1WfSpec(BaseWfSpec):
    def create_structure(self):
        a_task, b_task, c_task = [None] * 3
        for task in self.type_tasks_dict['simple']:
            if task.identification_name == 'A': a_task = task
            if task.identification_name == 'B': b_task = task
            if task.identification_name == 'C': c_task = task
        self.start_wf_task.connect(a_task)
        a_task.connect(b_task)
        b_task.connect(c_task)


class InnerBigTestWfSpec(BaseWfSpec):
    def create_structure(self):
        h_task, k_task = [None] * 2
        for task in self.type_tasks_dict['simple']:
            if task.identification_name == 'H': h_task = task
            if task.identification_name == 'K': k_task = task
        self.start_wf_task.connect(h_task)
        h_task.connect(k_task)


class MediumBigTestWfSpec(BaseWfSpec):
    def create_structure(self):
        f_task, g_task = [None] * 2
        for task in self.type_tasks_dict['simple']:
            if task.identification_name == 'F': f_task = task
            if task.identification_name == 'G': g_task = task

        inner_subwf_task = self.type_tasks_dict[WfConstants.SUBWF][0]
        self.start_wf_task.connect(f_task)
        f_task.connect(g_task)
        g_task.connect(inner_subwf_task)


class BigTest2WfSpec(BaseWfSpec):
    def create_structure(self):
        a_task, b_task, m_task, n_task, o_task, p_task, q_task, r_task = [None] * 8
        for task in self.type_tasks_dict['simple']:
            if task.identification_name == 'A': a_task = task
            if task.identification_name == 'B': b_task = task
            if task.identification_name == 'M': m_task = task
            if task.identification_name == 'N': n_task = task
            if task.identification_name == 'O': o_task = task
            if task.identification_name == 'P': p_task = task
            if task.identification_name == 'Q': q_task = task
            if task.identification_name == 'R': r_task = task
        medium_subwf = self.type_tasks_dict[WfConstants.SUBWF][0]
        join = self.create_join_task('join')
        choice_task = self.type_tasks_dict[WfConstants.CHOICE][0]

        self.start_wf_task.connect(a_task)
        self.start_wf_task.connect(medium_subwf)
        a_task.connect(b_task)
        b_task.connect(join)
        medium_subwf.connect(join)
        join.connect(m_task)
        m_task.connect(choice_task)
        choice_task.connect(n_task)
        choice_task.connect(p_task)
        choice_task.connect(q_task)
        n_task.connect(o_task)
        p_task.connect(r_task)
        q_task.connect(r_task)
