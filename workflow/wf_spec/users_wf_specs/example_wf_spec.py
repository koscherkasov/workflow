from workflow.wf_spec.base_wf_spec import BaseWfSpec
from workflow.wf_state import WfConstants


class ExampleWfSpec(BaseWfSpec):

    def create_structure(self):
        first_task = self.type_tasks_dict['first'][0]
        second_task = self.type_tasks_dict['should_approve_type'][0]
        third_task = self.type_tasks_dict['third'][0]
        self.start_wf_task.connect(first_task)
        first_task.connect(second_task)
        second_task.connect(third_task)


class ExampleJoinWfSpec(BaseWfSpec):

    def create_structure(self):
        first_task = self.type_tasks_dict['first'][0]
        second_task = self.type_tasks_dict['should_approve_type'][0]
        third_task = self.type_tasks_dict['third'][0]
        fourth_task = self.type_tasks_dict['fourth'][0]
        self.start_wf_task.connect(first_task)
        first_task.connect(second_task)
        first_task.connect(third_task)
        join1 = self.create_join_task('join1')
        second_task.connect(join1)
        third_task.connect(join1)
        join1.connect(fourth_task)


class ExampleChoiceWfSpec(BaseWfSpec):
    """
    self.type_tasks_dict is dict with key = str type and value = list of instances Task
    """
    def create_structure(self):
        first_task = self.type_tasks_dict['simple1'][0]
        choice_task = self.type_tasks_dict[WfConstants.CHOICE][0]
        second_task = self.type_tasks_dict['simple2'][0]
        third_task = self.type_tasks_dict['should_approve_type'][0]
        fourth_task = self.type_tasks_dict['simple4'][0]
        fifth_task = self.type_tasks_dict['simple5'][0]

        self.start_wf_task.connect(first_task)
        first_task.connect(choice_task)
        choice_task.connect(second_task)
        choice_task.connect(third_task)
        choice_task.connect(fourth_task)

        second_task.connect(fifth_task)
        third_task.connect(fifth_task)
        fourth_task.connect(fifth_task)