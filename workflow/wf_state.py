

class WfConstants(object):
    # state constants
    MAYBE = 1
    LIKELY = 2
    FUTURE = 4
    WAITING = 8
    READY = 16
    COMPLETED = 32
    CANCELLED = 64

    # TODO REFACTOR IT
    FINISHED_MASK = CANCELLED | COMPLETED
    DEFINITE_MASK = FUTURE | WAITING | READY | FINISHED_MASK
    PREDICTED_MASK = FUTURE | LIKELY | MAYBE
    NOT_FINISHED_MASK = PREDICTED_MASK | WAITING | READY
    ANY_MASK = FINISHED_MASK | NOT_FINISHED_MASK

    # service type constants
    START = 'start'
    JOIN = 'join'
    SUBWF = 'subwf'
    CHOICE = 'choice'
