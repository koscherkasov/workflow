from workflow.spec_factory.abstract_spec_factory import AbstractSpecFactory
from workflow.task_spec.users_task_specs.approve_task_spec import ApproveTaskSpec, NotApproveTaskSpec
from workflow.task_spec.users_task_specs.example_choice_task_spec import ExampleChoiceTaskSpec, ExampleChoiceTaskSpec1, \
    BigTestChoiceTaskSpec
from workflow.wf_spec.users_wf_specs.big_test_wf_spec import BigTest1WfSpec, InnerBigTestWfSpec, MediumBigTestWfSpec, \
    BigTest2WfSpec
from workflow.wf_spec.users_wf_specs.example_wf_spec import ExampleWfSpec, ExampleJoinWfSpec, ExampleChoiceWfSpec
from workflow.wf_spec.users_wf_specs.subwf_wf_spec import InnerWfSpec
from workflow.wf_spec.users_wf_specs.subwf_wf_spec import OuterWfSpec


class TestSpecFactory(AbstractSpecFactory):
    @classmethod
    def get_default_task_spec(cls):
        return NotApproveTaskSpec()

    @classmethod
    def get_registered_task_spec(cls, task_type, task_sub_type=None):
        registered_task_specs = {
            ('simple', 'approve'): ApproveTaskSpec(),
            ('should_approve_type', None): ApproveTaskSpec(),
        }
        return registered_task_specs.get((task_type, task_sub_type))

    @classmethod
    def get_registered_wf_spec(cls, wf_spec_name):
        registered_wf_spec = {
            'example_wf_spec': ExampleWfSpec,
            'example_join_wf_spec': ExampleJoinWfSpec,
            'outer_wf_spec': OuterWfSpec,
            'inner_wf_spec': InnerWfSpec,
            'example_choice_wf_spec': ExampleChoiceWfSpec,
            'wf_big_test1': BigTest1WfSpec,
            'sub_wf_inner_big_test': InnerBigTestWfSpec,
            'sub_wf_medium_big_test': MediumBigTestWfSpec,
            'wf_big_test2': BigTest2WfSpec
        }
        return registered_wf_spec.get(wf_spec_name)

    @classmethod
    def get_registered_choice_task_spec(cls, choice_spec_name):
        registered_choice_task_spec = {
            'test_choice_name': ExampleChoiceTaskSpec(),
            'test_choice_name1': ExampleChoiceTaskSpec1(),
            'big_test_choice_name': BigTestChoiceTaskSpec()
        }
        return registered_choice_task_spec.get(choice_spec_name)
