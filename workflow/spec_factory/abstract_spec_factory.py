from workflow.task_spec.service_tasks_specs.choice_task_spec import ChoiceTaskSpec
from workflow.task_spec.service_tasks_specs.join_task_spec import JoinTaskSpec
from workflow.task_spec.service_tasks_specs.start_task_spec import StartTaskSpec
from workflow.task_spec.service_tasks_specs.subwf_task_spec import SubwfTaskSpec
from workflow.wf_state import WfConstants


class AbstractSpecFactory(object):
    service_tasks_specs = {WfConstants.START: StartTaskSpec(),
                           WfConstants.JOIN: JoinTaskSpec(),
                           WfConstants.SUBWF: SubwfTaskSpec(),
                           WfConstants.CHOICE: ChoiceTaskSpec(),
                           }

    @classmethod
    def get_registered_choice_task_spec(cls, choice_spec_name):
        raise NotImplementedError()

    @classmethod
    def get_default_task_spec(cls):
        raise NotImplementedError()

    @classmethod
    def get_registered_task_spec(cls, task_type, task_sub_type=None):
        raise NotImplementedError()

    @classmethod
    def get_registered_wf_spec(cls, wf_spec_name):
        raise NotImplementedError()

    @classmethod
    def get_wf_spec(cls, wf_spec_name):
        wf_spec = cls.get_registered_wf_spec(wf_spec_name)
        if not wf_spec:
            raise WfSpecNotRegistered('WfSpec with name {} is not registered'.format(wf_spec_name))
        return wf_spec()

    @classmethod
    def get_task_spec(cls, task):
        service_type = task.service_type
        if service_type == WfConstants.CHOICE:
            return cls.get_registered_choice_task_spec(task.identification_name)
        if service_type:
            return cls.service_tasks_specs[service_type]
        task_spec = cls.get_registered_task_spec(task.type, task.sub_type)
        if not task_spec:
            task_spec = cls.get_registered_task_spec(task.type)
        if not task_spec:
            task_spec = cls.get_default_task_spec()
        return task_spec

    @classmethod
    def get_choice_spec(cls, choice_spec_name):
        choice_spec = cls.get_registered_wf_spec(choice_spec_name)
        if not choice_spec:
            raise ChoiceSpecNotRegistered('ChoiceSpec with name {} is not registered'.format(choice_spec_name))
        return choice_spec


class WfSpecNotRegistered(Exception):
    pass


class ChoiceSpecNotRegistered(Exception):
    pass
