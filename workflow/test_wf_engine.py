import datetime

import dependency_injector.providers as providers
from sqlalchemy import create_engine
from workflow.wf_engine import WfEngine, WfConstants, TaskInfo


def test_simple():
    # CODE = '123456789qwertyz'
    list_task_information = [
        TaskInfo(type='first', identification_name='A'),
        TaskInfo(type='should_approve_type', sub_type='second_sub', identification_name='B'),
        TaskInfo(type='third', identification_name='C')
    ]

    wf_id = WfEngine.create_wf(wf_spec_name='example_wf_spec',
                               list_task_information=list_task_information,
                               name='WF_name',
                               factory_name='test',
                               # object_code=CODE
                               )
    # wf_id = WfEngine.get_wf_by_object_code(CODE).id
    ready_tasks = WfEngine._get_ready_tasks(wf_id)
    assert len(ready_tasks) == 1
    result = WfEngine.start_wf(wf_id)
    assert result == WfConstants.WAITING
    task_b_id = WfEngine.get_tasks_by_name(wf_id, 'B')[0].id
    WfEngine.cancel(wf_id, task_b_id)
    assert WfEngine.get_tasks_by_name(wf_id, 'B')[0].state == WfConstants.CANCELLED
    WfEngine.run_cancelled_task(wf_id, task_b_id)
    assert WfEngine.get_tasks_by_name(wf_id, 'B')[0].state == WfConstants.WAITING
    WfEngine.approve(wf_id, task_b_id)
    assert WfEngine.is_finished(wf_id) == True


def test_join():
    list_task_information = [
        TaskInfo(type='first', identification_name='A'),
        TaskInfo(type='should_approve_type', sub_type='second_sub', identification_name='B'),
        TaskInfo(type='third', identification_name='C'),
        TaskInfo(type='fourth', identification_name='D')
    ]

    wf_id = WfEngine.create_wf(wf_spec_name='example_join_wf_spec',
                               list_task_information=list_task_information,
                               name='WF_name',
                               factory_name='test')
    ready_tasks = WfEngine._get_ready_tasks(wf_id)
    assert len(ready_tasks) == 1
    assert WfEngine.get_tasks_by_name(wf_id, 'join1')[0].state == WfConstants.FUTURE
    result = WfEngine.start_wf(wf_id)
    assert result == WfConstants.WAITING
    assert WfEngine.get_tasks_by_name(wf_id, 'join1')[0].state == WfConstants.WAITING
    task_b = WfEngine.get_tasks_by_name(wf_id, 'B')[0]
    assert WfEngine.get_tasks_by_name(wf_id, 'B')[0].state == WfConstants.WAITING
    WfEngine.approve(wf_id, task_b.id)
    assert WfEngine.is_finished(wf_id) == True


def test_subwf_inner_approve():
    inner_list_task_information = [
        TaskInfo(type='first', identification_name='inner_A'),
        TaskInfo(type='should_approve_type', sub_type='second_sub', identification_name='inner_B')
    ]

    subwf_id = WfEngine.create_wf(wf_spec_name='inner_wf_spec',
                                  list_task_information=inner_list_task_information,
                                  name='sub_WF_name',
                                  factory_name='test')

    list_task_information = [
        TaskInfo(type='first', identification_name='A'),
        TaskInfo(type='should_approve_type', sub_type='second_sub', identification_name='B'),
        TaskInfo(type='third', identification_name='C'),
        TaskInfo(type='fourth', identification_name='D'),
        TaskInfo(type=WfConstants.SUBWF, identification_name='sub_wf_task', subwf_id=subwf_id)
    ]

    wf_id = WfEngine.create_wf(wf_spec_name='outer_wf_spec',
                               list_task_information=list_task_information,
                               name='WF_name',
                               factory_name='test')

    result = WfEngine.start_wf(wf_id)
    assert result == WfConstants.WAITING
    task_b = WfEngine.get_tasks_by_name(wf_id, 'B')[0]
    WfEngine.approve(wf_id, task_b.id)
    task_subwf = WfEngine.get_tasks_by_name(wf_id, 'sub_wf_task')[0]

    subwf_id = task_subwf.subwf_id
    task_inner_b = WfEngine.get_tasks_by_name(subwf_id, 'inner_B')[0]
    assert task_subwf.state == WfConstants.WAITING
    assert task_inner_b.state == WfConstants.WAITING

    WfEngine.cancel(subwf_id, task_inner_b.id)
    assert WfEngine.get_tasks_by_name(subwf_id, 'inner_B')[0].state == WfConstants.CANCELLED
    assert WfEngine.get_tasks_by_name(wf_id, 'sub_wf_task')[0].state == WfConstants.CANCELLED

    WfEngine.run_cancelled_task(subwf_id, task_inner_b.id)
    assert WfEngine.get_tasks_by_name(subwf_id, 'inner_B')[0].state == WfConstants.WAITING
    assert WfEngine.get_tasks_by_name(wf_id, 'sub_wf_task')[0].state == WfConstants.WAITING

    WfEngine.approve(subwf_id, task_inner_b.id)
    assert WfEngine.get_tasks_by_name(subwf_id, 'inner_B')[0].state == WfConstants.COMPLETED
    assert WfEngine.get_tasks_by_name(wf_id, 'sub_wf_task')[0].state == WfConstants.COMPLETED
    assert WfEngine.is_finished(wf_id) == True


def test_subwf_outer_approve():
    inner_list_task_information = [
        TaskInfo(type='first', identification_name='inner_A'),
        TaskInfo(type='should_approve_type', sub_type='second_sub', identification_name='inner_B')
    ]

    subwf_id = WfEngine.create_wf(wf_spec_name='inner_wf_spec',
                                  list_task_information=inner_list_task_information,
                                  name='sub_WF_name',
                                  factory_name='test')

    list_task_information = [
        TaskInfo(type='first', identification_name='A'),
        TaskInfo(type='should_approve_type', sub_type='second_sub', identification_name='B'),
        TaskInfo(type='third', identification_name='C'),
        TaskInfo(type='fourth', identification_name='D'),
        TaskInfo(type=WfConstants.SUBWF, identification_name='sub_wf_task', subwf_id=subwf_id)
    ]

    wf_id = WfEngine.create_wf(wf_spec_name='outer_wf_spec',
                               list_task_information=list_task_information,
                               name='WF_name',
                               factory_name='test')

    result = WfEngine.start_wf(wf_id)
    assert result == WfConstants.WAITING
    task_b = WfEngine.get_tasks_by_name(wf_id, 'B')[0]
    WfEngine.approve(wf_id, task_b.id)
    task_subwf = WfEngine.get_tasks_by_name(wf_id, 'sub_wf_task')[0]

    subwf_id = task_subwf.subwf_id
    task_inner_b = WfEngine.get_tasks_by_name(subwf_id, 'inner_B')[0]
    assert task_subwf.state == WfConstants.WAITING
    assert task_inner_b.state == WfConstants.WAITING

    WfEngine.cancel(subwf_id, task_inner_b.id)
    assert WfEngine.get_tasks_by_name(subwf_id, 'inner_B')[0].state == WfConstants.CANCELLED
    assert WfEngine.get_tasks_by_name(wf_id, 'sub_wf_task')[0].state == WfConstants.CANCELLED

    WfEngine.run_cancelled_task(wf_id, task_subwf.id)
    assert WfEngine.get_tasks_by_name(subwf_id, 'inner_B')[0].state == WfConstants.WAITING
    assert WfEngine.get_tasks_by_name(wf_id, 'sub_wf_task')[0].state == WfConstants.WAITING

    WfEngine.approve(subwf_id, task_inner_b.id)
    assert WfEngine.get_tasks_by_name(subwf_id, 'inner_B')[0].state == WfConstants.COMPLETED
    assert WfEngine.get_tasks_by_name(wf_id, 'sub_wf_task')[0].state == WfConstants.COMPLETED
    assert WfEngine.is_finished(wf_id) == True


def test_choice_simple():
    list_task_information = [
        TaskInfo(type='simple1', identification_name='A'),
        TaskInfo(type=WfConstants.CHOICE, identification_name='test_choice_name'),
        TaskInfo(type='simple2', identification_name='B'),
        TaskInfo(type='should_approve_type', identification_name='C'),
        TaskInfo(type='simple4', identification_name='D'),
        TaskInfo(type='simple5', identification_name='E')
    ]
    wf_id = WfEngine.create_wf(wf_spec_name='example_choice_wf_spec',
                               list_task_information=list_task_information,
                               name='WF_name',
                               factory_name='test')
    WfEngine.start_wf(wf_id)
    assert WfEngine.get_tasks_by_name(wf_id, 'A')[0].state == WfConstants.COMPLETED
    assert WfEngine.get_tasks_by_name(wf_id, 'B')[0].state == WfConstants.COMPLETED
    assert WfEngine.get_tasks_by_name(wf_id, 'C')[0].state == WfConstants.FUTURE
    assert WfEngine.get_tasks_by_name(wf_id, 'D')[0].state == WfConstants.FUTURE
    assert WfEngine.get_tasks_by_name(wf_id, 'E')[0].state == WfConstants.COMPLETED
    assert WfEngine.is_finished(wf_id) == True


def test_choice_with_approve():
    list_task_information = [
        TaskInfo(type='simple1', identification_name='A'),
        TaskInfo(type=WfConstants.CHOICE, identification_name='test_choice_name1'),
        TaskInfo(type='simple2', identification_name='B'),
        TaskInfo(type='should_approve_type', identification_name='C'),
        TaskInfo(type='simple4', identification_name='D'),
        TaskInfo(type='simple5', identification_name='E')
    ]
    wf_id = WfEngine.create_wf(wf_spec_name='example_choice_wf_spec',
                               list_task_information=list_task_information,
                               name='WF_name',
                               factory_name='test')
    WfEngine.start_wf(wf_id)
    assert WfEngine.get_tasks_by_name(wf_id, 'A')[0].state == WfConstants.COMPLETED
    assert WfEngine.get_tasks_by_name(wf_id, 'B')[0].state == WfConstants.FUTURE
    assert WfEngine.get_tasks_by_name(wf_id, 'C')[0].state == WfConstants.WAITING
    assert WfEngine.get_tasks_by_name(wf_id, 'D')[0].state == WfConstants.FUTURE
    assert WfEngine.get_tasks_by_name(wf_id, 'E')[0].state == WfConstants.FUTURE
    assert WfEngine.is_finished(wf_id) == False

    task_c = WfEngine.get_tasks_by_name(wf_id, 'C')[0]
    WfEngine.approve(wf_id, task_c.id)
    assert WfEngine.get_tasks_by_name(wf_id, 'A')[0].state == WfConstants.COMPLETED
    assert WfEngine.get_tasks_by_name(wf_id, 'B')[0].state == WfConstants.FUTURE
    assert WfEngine.get_tasks_by_name(wf_id, 'C')[0].state == WfConstants.COMPLETED
    assert WfEngine.get_tasks_by_name(wf_id, 'D')[0].state == WfConstants.FUTURE
    assert WfEngine.get_tasks_by_name(wf_id, 'E')[0].state == WfConstants.COMPLETED
    assert WfEngine.is_finished(wf_id) == True


def test_associate1():
    """
    CREATE1
    CREATE2
    START1
    START2
    APPROVE
    """
    list_task_information1 = [
        TaskInfo(type='first', identification_name='A1'),
        TaskInfo(type='should_approve_type', identification_name='B1'),
        TaskInfo(type='third', identification_name='C1'),
    ]

    wf_id1 = WfEngine.create_wf(wf_spec_name='example_wf_spec',
                                list_task_information=list_task_information1,
                                name='WF_name1',
                                factory_name='test')
    task_a1 = WfEngine.get_tasks_by_name(wf_id1, 'A1')[0]
    task_b1 = WfEngine.get_tasks_by_name(wf_id1, 'B1')[0]

    list_task_information2 = [
        TaskInfo(type='first', associate_with_wf_task_id=task_a1.id),
        TaskInfo(type='should_approve_type', associate_with_wf_task_id=task_b1.id),
        TaskInfo(type='third', identification_name='C2'),
    ]

    wf_id2 = WfEngine.create_wf(wf_spec_name='example_wf_spec',
                                list_task_information=list_task_information2,
                                name='WF_name1',
                                factory_name='test')

    WfEngine.start_wf(wf_id1)
    WfEngine.start_wf(wf_id2)
    assert len(WfEngine.get_wf_by_id(wf_id1).wf_tasks) == 4
    assert len(WfEngine.get_wf_by_id(wf_id2).wf_tasks) == 4
    assert WfEngine.get_tasks_by_name(wf_id1, 'A1')[0].state == WfConstants.COMPLETED
    assert WfEngine.get_tasks_by_name(wf_id2, 'A1')[0].state == WfConstants.COMPLETED
    assert WfEngine.get_tasks_by_name(wf_id1, 'B1')[0].state == WfConstants.WAITING
    assert WfEngine.get_tasks_by_name(wf_id2, 'B1')[0].state == WfConstants.WAITING

    WfEngine.approve(wf_id1, task_b1.id)
    assert WfEngine.get_tasks_by_name(wf_id1, 'B1')[0].state == WfConstants.COMPLETED
    assert WfEngine.get_tasks_by_name(wf_id2, 'B1')[0].state == WfConstants.COMPLETED
    assert WfEngine.get_tasks_by_name(wf_id1, 'C1')[0].state == WfConstants.COMPLETED
    assert WfEngine.get_tasks_by_name(wf_id2, 'C2')[0].state == WfConstants.COMPLETED
    assert WfEngine.is_finished(wf_id1) == True
    assert WfEngine.is_finished(wf_id2) == True


def test_associate2():
    """
    CREATE1
    START1
    CREATE2
    START2
    APPROVE
    """
    list_task_information1 = [
        TaskInfo(type='first', identification_name='A1'),
        TaskInfo(type='should_approve_type', identification_name='B1'),
        TaskInfo(type='third', identification_name='C1'),
    ]

    wf_id1 = WfEngine.create_wf(wf_spec_name='example_wf_spec',
                                list_task_information=list_task_information1,
                                name='WF_name1',
                                factory_name='test')
    WfEngine.start_wf(wf_id1)

    task_a1 = WfEngine.get_tasks_by_name(wf_id1, 'A1')[0]
    task_b1 = WfEngine.get_tasks_by_name(wf_id1, 'B1')[0]

    list_task_information2 = [
        TaskInfo(type='first', associate_with_wf_task_id=task_a1.id),
        TaskInfo(type='should_approve_type', associate_with_wf_task_id=task_b1.id),
        TaskInfo(type='third', identification_name='C2'),
    ]

    wf_id2 = WfEngine.create_wf(wf_spec_name='example_wf_spec',
                                list_task_information=list_task_information2,
                                name='WF_name1',
                                factory_name='test')
    WfEngine.start_wf(wf_id2)
    assert len(WfEngine.get_wf_by_id(wf_id1).wf_tasks) == 4
    assert len(WfEngine.get_wf_by_id(wf_id2).wf_tasks) == 4
    assert WfEngine.get_tasks_by_name(wf_id1, 'A1')[0].state == WfConstants.COMPLETED
    assert WfEngine.get_tasks_by_name(wf_id2, 'A1')[0].state == WfConstants.COMPLETED
    assert WfEngine.get_tasks_by_name(wf_id1, 'B1')[0].state == WfConstants.WAITING
    assert WfEngine.get_tasks_by_name(wf_id2, 'B1')[0].state == WfConstants.WAITING
    WfEngine.approve(wf_id1, task_b1.id)
    assert WfEngine.get_tasks_by_name(wf_id1, 'B1')[0].state == WfConstants.COMPLETED
    assert WfEngine.get_tasks_by_name(wf_id2, 'B1')[0].state == WfConstants.COMPLETED
    assert WfEngine.get_tasks_by_name(wf_id1, 'C1')[0].state == WfConstants.COMPLETED
    assert WfEngine.get_tasks_by_name(wf_id2, 'C2')[0].state == WfConstants.COMPLETED
    assert WfEngine.is_finished(wf_id1) == True
    assert WfEngine.is_finished(wf_id2) == True


def test_associate3():
    """
    CREATE1
    START1
    APPROVE1
    CREATE2
    START2
    """
    list_task_information1 = [
        TaskInfo(type='first', identification_name='A1'),
        TaskInfo(type='should_approve_type', identification_name='B1'),
        TaskInfo(type='third', identification_name='C1'),
    ]

    wf_id1 = WfEngine.create_wf(wf_spec_name='example_wf_spec',
                                list_task_information=list_task_information1,
                                name='WF_name1',
                                factory_name='test')
    WfEngine.start_wf(wf_id1)
    assert len(WfEngine.get_wf_by_id(wf_id1).wf_tasks) == 4
    assert WfEngine.get_tasks_by_name(wf_id1, 'A1')[0].state == WfConstants.COMPLETED
    assert WfEngine.get_tasks_by_name(wf_id1, 'B1')[0].state == WfConstants.WAITING

    task_a1 = WfEngine.get_tasks_by_name(wf_id1, 'A1')[0]
    task_b1 = WfEngine.get_tasks_by_name(wf_id1, 'B1')[0]
    WfEngine.approve(wf_id1, task_b1.id)
    assert WfEngine.get_tasks_by_name(wf_id1, 'B1')[0].state == WfConstants.COMPLETED
    assert WfEngine.get_tasks_by_name(wf_id1, 'C1')[0].state == WfConstants.COMPLETED
    assert WfEngine.is_finished(wf_id1) == True

    list_task_information2 = [
        TaskInfo(type='first', associate_with_wf_task_id=task_a1.id),
        TaskInfo(type='should_approve_type', associate_with_wf_task_id=task_b1.id),
        TaskInfo(type='third', identification_name='C2'),
    ]

    wf_id2 = WfEngine.create_wf(wf_spec_name='example_wf_spec',
                                list_task_information=list_task_information2,
                                name='WF_name1',
                                factory_name='test')
    WfEngine.start_wf(wf_id2)
    assert WfEngine.get_tasks_by_name(wf_id2, 'C2')[0].state == WfConstants.COMPLETED
    assert WfEngine.is_finished(wf_id2) == True


def big_test():
    """
    https://realtimeboard.com/app/board/o9J_k0rxtyE=/?moveToWidget=3074457345746752279
    """
    list_task_information1 = [
        TaskInfo(type='simple', identification_name='A'),
        TaskInfo(type='simple', sub_type='approve', identification_name='B'),
        TaskInfo(type='simple', identification_name='C'),
    ]

    wf_id1 = WfEngine.create_wf(wf_spec_name='wf_big_test1',
                                list_task_information=list_task_information1,
                                name='WF_name1',
                                factory_name='test')
    WfEngine.start_wf(wf_id1)
    assert len(WfEngine.get_wf_by_id(wf_id1).wf_tasks) == 4
    assert WfEngine.get_tasks_by_name(wf_id1, 'A')[0].state == WfConstants.COMPLETED
    assert WfEngine.get_tasks_by_name(wf_id1, 'B')[0].state == WfConstants.WAITING

    task_a = WfEngine.get_tasks_by_name(wf_id1, 'A')[0]
    task_b = WfEngine.get_tasks_by_name(wf_id1, 'B')[0]
    WfEngine.approve(wf_id1, task_b.id)
    assert WfEngine.get_tasks_by_name(wf_id1, 'B')[0].state == WfConstants.COMPLETED
    assert WfEngine.get_tasks_by_name(wf_id1, 'C')[0].state == WfConstants.COMPLETED
    assert WfEngine.is_finished(wf_id1) == True

    inner_list_task_information = [
        TaskInfo(type='simple', identification_name='H'),
        TaskInfo(type='simple', sub_type='approve', identification_name='K')
    ]

    inner_subwf_id = WfEngine.create_wf(wf_spec_name='sub_wf_inner_big_test',
                                        list_task_information=inner_list_task_information,
                                        name='sub_WF_inner',
                                        factory_name='test')

    medium_list_task_information = [
        TaskInfo(type='simple', identification_name='F'),
        TaskInfo(type='simple', identification_name='G'),
        TaskInfo(type=WfConstants.SUBWF, identification_name='sub_wf_inner_task', subwf_id=inner_subwf_id)
    ]

    medium_subwf_id = WfEngine.create_wf(wf_spec_name='sub_wf_medium_big_test',
                                         list_task_information=medium_list_task_information,
                                         name='sub_WF_medium',
                                         factory_name='test')

    list_task_information2 = [
        TaskInfo(type='simple', associate_with_wf_task_id=task_a.id),
        TaskInfo(type='simple', associate_with_wf_task_id=task_b.id),
        TaskInfo(type=WfConstants.SUBWF, identification_name='sub_wf_medium_task', subwf_id=medium_subwf_id),
        TaskInfo(type='simple', identification_name='M'),
        TaskInfo(type=WfConstants.CHOICE, identification_name='big_test_choice_name'),
        TaskInfo(type='simple', identification_name='N'),
        TaskInfo(type='simple', identification_name='O'),
        TaskInfo(type='simple', sub_type='approve', identification_name='P'),
        TaskInfo(type='simple', identification_name='Q'),
        TaskInfo(type='simple', identification_name='R')
    ]

    wf_id2 = WfEngine.create_wf(wf_spec_name='wf_big_test2',
                                list_task_information=list_task_information2,
                                name='sub_WF_medium',
                                factory_name='test')

    WfEngine.start_wf(wf_id2)
    assert WfEngine.get_tasks_by_name(inner_subwf_id, 'H')[0].state == WfConstants.COMPLETED
    assert WfEngine.get_tasks_by_name(inner_subwf_id, 'K')[0].state == WfConstants.WAITING
    assert WfEngine.get_tasks_by_name(medium_subwf_id, 'sub_wf_inner_task')[0].state == WfConstants.WAITING
    assert WfEngine.get_tasks_by_name(wf_id2, 'sub_wf_medium_task')[0].state == WfConstants.WAITING

    task_k =WfEngine.get_tasks_by_name(inner_subwf_id, 'K')[0]
    WfEngine.cancel(inner_subwf_id, task_k.id)
    assert WfEngine.get_tasks_by_name(inner_subwf_id, 'K')[0].state == WfConstants.CANCELLED
    assert WfEngine.get_tasks_by_name(medium_subwf_id, 'sub_wf_inner_task')[0].state == WfConstants.CANCELLED
    assert WfEngine.get_tasks_by_name(wf_id2, 'sub_wf_medium_task')[0].state == WfConstants.CANCELLED

    sub_wf_inner_task = WfEngine.get_tasks_by_name(medium_subwf_id, 'sub_wf_inner_task')[0]
    WfEngine.run_cancelled_task(medium_subwf_id, sub_wf_inner_task.id)
    WfEngine.approve(inner_subwf_id, task_k.id)

    now = datetime.datetime.now()
    result_way_dict = {0: 'N', 1: 'P', 2: 'Q'}
    result_number = now.hour % 3
    result_way = result_way_dict[result_number]
    print('result_way = {}'.format(result_way))
    if result_way == 'N':
        assert WfEngine.get_tasks_by_name(wf_id2, 'N')[0].state == WfConstants.COMPLETED
        assert WfEngine.get_tasks_by_name(wf_id2, 'O')[0].state == WfConstants.COMPLETED
        assert WfEngine.get_tasks_by_name(wf_id2, 'P')[0].state == WfConstants.FUTURE
        assert WfEngine.get_tasks_by_name(wf_id2, 'Q')[0].state == WfConstants.FUTURE
        assert WfEngine.get_tasks_by_name(wf_id2, 'R')[0].state == WfConstants.FUTURE
        assert WfEngine.is_finished(wf_id2) == True
    if result_way == 'Q':
        assert WfEngine.get_tasks_by_name(wf_id2, 'N')[0].state == WfConstants.FUTURE
        assert WfEngine.get_tasks_by_name(wf_id2, 'O')[0].state == WfConstants.FUTURE
        assert WfEngine.get_tasks_by_name(wf_id2, 'P')[0].state == WfConstants.FUTURE
        assert WfEngine.get_tasks_by_name(wf_id2, 'Q')[0].state == WfConstants.COMPLETED
        assert WfEngine.get_tasks_by_name(wf_id2, 'R')[0].state == WfConstants.COMPLETED
        assert WfEngine.is_finished(wf_id2) == True
    if result_way == 'P':
        assert WfEngine.get_tasks_by_name(wf_id2, 'N')[0].state == WfConstants.FUTURE
        assert WfEngine.get_tasks_by_name(wf_id2, 'O')[0].state == WfConstants.FUTURE
        assert WfEngine.get_tasks_by_name(wf_id2, 'P')[0].state == WfConstants.WAITING
        assert WfEngine.get_tasks_by_name(wf_id2, 'Q')[0].state == WfConstants.FUTURE
        assert WfEngine.get_tasks_by_name(wf_id2, 'R')[0].state == WfConstants.FUTURE

        p_task = WfEngine.get_tasks_by_name(wf_id2, 'P')[0]
        WfEngine.approve(wf_id2, p_task.id)
        assert WfEngine.get_tasks_by_name(wf_id2, 'P')[0].state == WfConstants.COMPLETED
        assert WfEngine.get_tasks_by_name(wf_id2, 'Q')[0].state == WfConstants.FUTURE
        assert WfEngine.get_tasks_by_name(wf_id2, 'R')[0].state == WfConstants.COMPLETED
        assert WfEngine.is_finished(wf_id2) == True


def start_tests():
    engine = create_engine('sqlite:///test-base.db')
    WfEngine.engine.override(engine)
    WfEngine.initialize()
    test_simple()
    test_join()
    test_subwf_inner_approve()
    test_subwf_outer_approve()
    test_choice_simple()
    test_choice_with_approve()
    test_associate1()
    test_associate2()
    test_associate3()
    big_test()
    print('All tests passed successfully')

if __name__ == '__main__':
    start_tests()
