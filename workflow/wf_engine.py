import dependency_injector.providers as providers
from sqlalchemy.orm.exc import NoResultFound, MultipleResultsFound

from workflow.model.wf_model import Workflow, WfTask, WorkflowSession
from workflow.spec_factory.abstract_spec_factory import AbstractSpecFactory
from workflow.spec_factory.registered_factory import registered_factory
from workflow.wf_state import WfConstants


class NotExistWfTask(Exception):
    """Exception when WfTask with given id is not exist"""
    pass


class NotExistWf(Exception):
    """Exception when Workflow with given id is not exist"""
    pass


class BadTaskStateException(Exception):
    """Exception when task has unpredictable state"""
    pass


class TaskNotFoundException(Exception):
    """Exception when workflow hasn't got task with given parameters"""
    pass


class FactoryNotRegistered(Exception):
    """Exception when factory with given name hasn't found"""
    pass


class NotInitializedError(Exception):
    """Exception when call method WfEngine.create_wf without initialization before"""
    pass


class TaskInfo(object):
    """Task information class with attributes and without any logic"""

    def __init__(self, type, sub_type=None, identification_name=None, subwf_id=None, associate_with_wf_task_id=None):
        self.type = type
        self.sub_type = sub_type
        self.identification_name = identification_name
        self.subwf_id = subwf_id
        self.associate_with_wf_task_id = associate_with_wf_task_id


class WfEngine(object):
    engine = providers.Provider()

    @classmethod
    def initialize(cls, engine=None, **kwargs):
        """Initialize WfEngine.
        If engine==None, before call this method override WfEngine.engine
        else NotImplementedError will be raised

            :param engine: SQLAlchemy's engine where workflow's tables will be created;
            :type engine: SQLAlchemy's engine;
            :param kwargs: dict with str key name for FactorySpec's name, value = instance of FactorySpec.
        """
        if not engine:
            try:
                cls.engine()
            except NotImplementedError:
                raise NotInitializedError('WfEngine.engine has not been overridden and param engine is None')
            WorkflowSession.engine.override(providers.Object(cls.engine()))
        else:
            WorkflowSession.engine.override(providers.Object(engine))
        for factory_name, factory_spec in kwargs.items():
            if not isinstance(factory_spec, AbstractSpecFactory):
                raise TypeError('{} is not AbstractSpecFactory class'.format(factory_spec))
            registered_factory[factory_name] = factory_spec

    @staticmethod
    def create_wf(wf_spec_name, list_task_information, name, factory_name, object_code=None):
        """Create workflow model and return id of created workflow model.

            :param wf_spec_name: Name of WorkflowSpec;
            :type wf_spec_name: str;
            :param list_task_information: flat information about tasks;
            :type list_task_information: list of instance TaskInfo;
            :param name: workflow's name;
            :type name: str;
            :param factory_name: factory's name which will be used;
            :type factory_name: str;
            :param object_code: objects code related with workflow;
            :type object_code: code;

            :return: id of created  workflow model.
        """
        try:
            WorkflowSession.engine()
        except NotImplementedError:
            raise NotInitializedError('WfEngine is not initialized')
        factory = registered_factory.get(factory_name)
        if not factory:
            raise FactoryNotRegistered('Factory with name {} is not registered'.format(factory_name))
        wf_spec = factory.get_wf_spec(wf_spec_name)
        session = WorkflowSession.get_session()
        wf_id = wf_spec.create_wf(list_task_information, name, factory_name, session, object_code)
        WorkflowSession.remove_scoped_session()
        return wf_id

    @staticmethod
    def get_wf_by_id(wf_id):
        """Get instance Workflow by id.

            :param wf_id: workflow's id;
            :type wf_id: int;

            :return: instance Workflow.
        """
        try:
            session = WorkflowSession.get_session()
            wf = session.query(Workflow).filter_by(id=wf_id).one()
            return wf
        except NoResultFound:
            raise NotExistWf('Workflow with id={} is not exist'.format(wf_id))

    @staticmethod
    def get_wf_by_object_code(object_code):
        """Get instance Workflow by object_code.

            :param object_code: workflow's object_code;
            :type object_code: int;

            :return: instance Workflow.
        """
        try:
            session = WorkflowSession.get_session()
            wf = session.query(Workflow).filter_by(object_code=object_code).one()
            return wf
        except NoResultFound:
            raise NotExistWf('Workflow with object_code={} is not exist'.format(object_code))

    @staticmethod
    def get_task_by_id(wf_task_id):
        """Get instance WfTask by id.

            :param wf_task_id: wf_tasks's id;
            :type wf_task_id: int;

            :return: instance WfTask.
        """
        try:
            session = WorkflowSession.get_session()
            wf = session.query(WfTask).filter_by(id=wf_task_id).one()
            return wf
        except NoResultFound:
            raise NotExistWfTask('WfTask with id={} is not exist'.format(wf_task_id))

    @staticmethod
    def start_wf(wf_id):
        """Start workflow by id.

            :param wf_id: workflow's id;
            :type wf_id: int;

            :return int If workflow has finished return WfConstants.COMPLETED
                    otherwise return WfConstants.WAITING.
        """
        while WfEngine._execute_ready_tasks(wf_id):
            pass
        WorkflowSession.remove_scoped_session()
        if WfEngine.is_finished(wf_id):
            return WfConstants.COMPLETED
        return WfConstants.WAITING

    @staticmethod
    def _get_ready_tasks(wf_id):
        """Get WfTasks with state WfConstants.READY.

            :param wf_id: workflow's id;
            :type wf_id: int;

            :return: list of instance WfTask.
        """
        wf = WfEngine.get_wf_by_id(wf_id)
        return wf.get_tasks(WfConstants.READY)

    @staticmethod
    def _get_not_completed_task(wf_id):
        """Get not completed WfTasks.

            :param wf_id: workflow's id;
            :type wf_id: int;

            :return: list of instance WfTask.
        """
        wf = WfEngine.get_wf_by_id(wf_id)
        not_completed_mask = WfConstants.NOT_FINISHED_MASK | WfConstants.CANCELLED
        return wf.get_tasks(not_completed_mask)

    @classmethod
    def _execute_ready_tasks(cls, wf_id):
        """Execute all tasks with state WfConstants.READY and update children
        It is the base unit of execution.
        Current method execute only ONE STAGE and return count completed task in current stage.

            :param wf_id: workflow's id;
            :type wf_id: int;

            :return: int Count completed task in current stage.
        """
        ready_tasks = WfEngine._get_ready_tasks(wf_id)
        wf = WfEngine.get_wf_by_id(wf_id)
        factory = registered_factory.get(wf.factory_name)
        comleted_task = 0
        if ready_tasks:
            should_update_tasks = []
            for task in ready_tasks:
                task_spec = factory.get_task_spec(task)
                result_state = task_spec.start_execution(task, wf_id, cls)
                if result_state is WfConstants.COMPLETED:
                    task.set_state(WfConstants.COMPLETED)
                    should_update_tasks.append(task)
                    comleted_task += 1
                else:
                    if result_state is WfConstants.WAITING:
                        task.set_state(WfConstants.WAITING)
                        WfEngine._update_parent_wfs(wf, WfConstants.WAITING)
            for task in should_update_tasks:
                task_spec = factory.get_task_spec(task)
                children_tasks_dict = {task: factory.get_task_spec(task) for task in task.children}
                task_spec.update_children(task, wf_id, children_tasks_dict, cls, factory)
        session = WorkflowSession.get_session()
        session.commit()
        return comleted_task

    @staticmethod
    def _continue_execution(wf_id):
        """Private method called for continue workflow after approve or run after cancelled.

            :param wf_id: workflow's id;
            :type wf_id: int.
        """
        while WfEngine._execute_ready_tasks(wf_id):
            pass
        WorkflowSession.remove_scoped_session()
        if WfEngine.is_finished(wf_id):
            wf = WfEngine.get_wf_by_id(wf_id)
            parents_tasks = wf.parent_tasks
            if parents_tasks:
                if len(parents_tasks) > 1:
                    raise MultipleResultsFound('Persistence is broken: multiple tasks with subwf_id {}'.format(wf_id))
                parent_task = parents_tasks[0]
                WfEngine.approve(parent_task.wfs[0].id, parent_task.id)

    @staticmethod
    def is_finished(wf_id):
        """Return true if At least one task without children has state WfConstants.COMPLETED.

            :param wf_id: workflow's id;
            :type wf_id: int;

            :return: boolean True if workflow is finished. Otherwise return False.
        """
        wf = WfEngine.get_wf_by_id(wf_id)
        tasks = wf.get_tasks()
        final_tasks = [task for task in tasks if not task.children]
        for task in final_tasks:
            if task.state == WfConstants.COMPLETED:
                return True
        return False

    @classmethod
    def approve(cls, wf_id, wf_task_id, **kwargs):
        """Approve waiting task.

            :param wf_id: workflow's id;
            :type wf_id: int;
            :param wf_task_id: wf_tasks's id;
            :type wf_task_id: int;
            :param kwargs: dict which is transmitted for task_spec.approve_hook.
        """
        wf = WfEngine.get_wf_by_id(wf_id)
        factory = registered_factory.get(wf.factory_name)
        task = WfEngine.get_task_by_id(wf_task_id)
        if task.state is not WfConstants.WAITING:
            raise BadTaskStateException('Workflow: {}. Task {} is not in'
                                        ' WAITING state'.format(wf_id, wf_task_id))
        task_spec = factory.get_task_spec(task)
        task_spec.approve_hook(task, wf_id, cls, **kwargs)
        task.set_state(WfConstants.COMPLETED)
        children_tasks_dict = {task: factory.get_task_spec(task) for task in task.children}
        task_spec.update_children(task, wf_id, children_tasks_dict, cls, factory)
        session = WorkflowSession.get_session()
        session.commit()
        for wf in task.wfs:
            WfEngine._continue_execution(wf.id)
        WorkflowSession.remove_scoped_session()

    @classmethod
    def cancel(cls, wf_id, wf_task_id, **kwargs):
        """Cancel waiting task.

            :param wf_id: workflow's id;
            :type wf_id: int;
            :param wf_task_id: wf_tasks's id;
            :type wf_task_id: int;
            :param kwargs: dict which is transmitted for task_spec.cancel_hook.
        """
        wf = WfEngine.get_wf_by_id(wf_id)
        factory = registered_factory.get(wf.factory_name)
        task = WfEngine.get_task_by_id(wf_task_id)
        if task.state is not WfConstants.WAITING:
            raise BadTaskStateException('Workflow: {}. Task {} is not in'
                                        ' WAITING state'.format(wf_id, wf_task_id))
        task.set_state(WfConstants.CANCELLED)
        task_spec = factory.get_task_spec(task)
        task_spec.cancel_hook(task, wf_id, cls, **kwargs)
        parents_tasks = wf.parent_tasks
        if parents_tasks:
            if len(parents_tasks) > 1:
                raise MultipleResultsFound('Persistence is broken: multiple tasks with subwf_id {}'.format(wf_id))
            parent_task = parents_tasks[0]
            WfEngine.cancel(parent_task.wfs[0].id, parent_task.id, **kwargs)
        session = WorkflowSession.get_session()
        session.commit()
        WorkflowSession.remove_scoped_session()

    @staticmethod
    def run_cancelled_task(wf_id, wf_task_id):
        """Run cancelled task.

            :param wf_id: workflow's id;
            :type wf_id: int;
            :param wf_task_id: wf_tasks's id;
            :type wf_task_id: int;
        """
        wf = WfEngine.get_wf_by_id(wf_id)
        task = WfEngine.get_task_by_id(wf_task_id)
        if task.state is not WfConstants.CANCELLED:
            raise BadTaskStateException('Workflow: {}. Task {} is not in'
                                        ' CANCELLED state'.format(wf_id, wf_task_id))
        task.set_state(WfConstants.READY)
        WfEngine._update_parent_wfs(wf, WfConstants.WAITING)
        session = WorkflowSession.get_session()
        session.commit()
        for wf in task.wfs:
            WfEngine._continue_execution(wf.id)
        WorkflowSession.remove_scoped_session()

    @staticmethod
    def _update_parent_wfs(wf, state):
        """Update all parents if exist to new state
        Currently this private method used when subworkflow starts after CANCELLED,
        and update parents state from CANCELLED to WAITING.

            :param wf: workflow;
            :type wf: instance of Workflow;
            :param state: one of task states from WfConstants class, f.e WfConstants.WAITING;
            :type state: int.
        """
        parents_tasks = wf.parent_tasks
        if parents_tasks:
            if len(parents_tasks) > 1:
                raise MultipleResultsFound('Persistence is broken: multiple tasks with subwf_id {}'.format(wf.id))
            parent_task = parents_tasks[0]
            parent_task.set_state(state)
            WfEngine._update_parent_wfs(parent_task.wfs[0], state)
        session = WorkflowSession.get_session()
        session.commit()

    @staticmethod
    def get_tasks_by_name(wf_id, identification_name):
        """Get list of tasks with identification_name from wf_id.

            :param wf_id: workflow's id;
            :type wf_id: int;
            :param identification_name: task's identification name;
            :type identification_name: str;

            :return: list of instance WfTask.
        """
        wf = WfEngine.get_wf_by_id(wf_id)
        tasks = []
        for task in wf.wf_tasks:
            if task.identification_name == identification_name:
                tasks.append(task)
        if tasks:
            return tasks
        raise TaskNotFoundException("Workflow with id = {} hasn't got task with identification_name={}".
                                    format(wf_id, identification_name))

    @staticmethod
    def get_task_by_params(wf_id, type, sub_type, name):
        """ Get task by type sub_type and identification_name.

            :param wf_id: workflow's id;
            :type wf_id: int;
            :param type: task's type;
            :type type: str;
            :param sub_type: task's sub_type;
            :type sub_type: str;
            :param name: task's identification name;
            :type name: str;

            :return: instance WfTask.
        """
        wf = WfEngine.get_wf_by_id(wf_id)
        for task in wf.wf_tasks:
            if task.type == type and task.sub_type == sub_type and task.identification_name == name:
                return task
        raise TaskNotFoundException("Workflow with id = {} hasn't got task with params: {} {} {}".
                                    format(wf_id, type, sub_type, name))
