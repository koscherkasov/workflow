import dependency_injector.providers as providers

from sqlalchemy import Column, Integer, BigInteger, ForeignKey, Unicode, Table
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session
from sqlalchemy.orm import sessionmaker, relationship, backref

DBSession = scoped_session(sessionmaker(expire_on_commit=False))
BaseMixin = type("BaseMixin", (object,), {"query": DBSession.query_property()})
Base = declarative_base(cls=BaseMixin)

wf_tasks_children = Table('wf_tasks_children', Base.metadata,
                          Column('wf_task_id', Integer, ForeignKey('wf_task.id')),
                          Column('child_wf_task_id', Integer, ForeignKey('wf_task.id'))
                          )

a_workflow_n_wf_task = Table('a_workflow_n_wf_task', Base.metadata,
                             Column('workflow_id', Integer, ForeignKey('workflow.id')),
                             Column('wf_task_id', Integer, ForeignKey('wf_task.id'))
                             )


class WfTask(Base):
    __tablename__ = 'wf_task'
    id = Column(Integer, primary_key=True, autoincrement=True)
    type = Column(Unicode(128))
    sub_type = Column(Unicode(128))
    identification_name = Column(Unicode(256))
    state = Column(Integer)
    service_type = Column(Unicode(32))
    subwf_id = Column(BigInteger, ForeignKey("workflow.id"))
    subwfs = relationship("Workflow", foreign_keys=[subwf_id], backref=backref('parent_tasks'))
    children = relationship(
        "WfTask",
        secondary=wf_tasks_children,
        primaryjoin=(wf_tasks_children.c.wf_task_id == id),
        secondaryjoin=(wf_tasks_children.c.child_wf_task_id == id),
        backref=backref('parents'))
    wfs = relationship(
        "Workflow",
        secondary=a_workflow_n_wf_task,
        back_populates="wf_tasks")

    def connect(self, other_wf_task):
        self.children.append(other_wf_task)

    def __repr__(self):
        return '<{}.{}.{}>'.format(self.__class__.__name__, self.id, self.identification_name)

    def set_state(self, state):
        self.state = state


class Workflow(Base):
    __tablename__ = 'workflow'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(Unicode(64))
    first_wf_task_id = Column(Integer, ForeignKey('wf_task.id'))
    first_wf_task = relationship("WfTask", foreign_keys=[first_wf_task_id])
    wf_tasks = relationship(
        "WfTask",
        secondary=a_workflow_n_wf_task,
        back_populates="wfs")
    factory_name = Column(Unicode(64))
    object_code = Column(Unicode(256), unique=True)

    def add_tasks(self, tasks):
        for task in tasks:
            self.wf_tasks.append(task)

    def __repr__(self):
        return '<{}.{}.{}>'.format(self.__class__.__name__, self.id, self.name)

    def get_tasks(self, state=None):
        if not state:
            return self.wf_tasks
        tasks = [task for task in self.wf_tasks if task.state == state]
        return tasks


class WorkflowSession:
    engine = providers.Provider()

    @classmethod
    def get_session(cls):
        if DBSession.registry.has():
            # get session
            return DBSession()
        # create session
        Base.metadata.bind = cls.engine()
        Base.metadata.create_all()
        DBSession.configure(bind=cls.engine())
        return DBSession()

    @classmethod
    def remove_scoped_session(cls):
        DBSession.remove()

# if __name__ == '__main__':
#     task1 = WfTask(identification_name='ololo')
#     task2 = WfTask(identification_name='agagag')
#     task3 = WfTask(identification_name='lmlmlmlml')
#     task4 = WfTask(identification_name='opoopopop')
#     task1.connect(task2)
#     task2.connect(task3)
#     task2.connect(task4)
#     wf = Workflow(name='my_wf', first_wf_task=task1)
#     wf.add_tasks([task1, task2, task3, task4])
#     wf1 = Workflow(name='my_wf1', first_wf_task=task1)
#     wf1.add_tasks([task1])
#     session = WorkflowSession.get_session()
#     session.add(task1)
#     session.add(task2)
#     session.add(task3)
#     session.add(task4)
#     session.commit()
#     WorkflowSession.get_session()
#     session.remove()
#     session = WorkflowSession.get_session()
#
#     obj = None
#     try:
#         obj = session.query(WfTask).filter_by(id=2).one()
#     except NoResultFound:
#         print("NoResultFound")
#     except MultipleResultsFound:
#         print("MultipleResultsFound")
#     print(obj)
#     print(obj.children)
#     print(obj.parents)
#
#     wfr = None
#     try:
#         wfr = session.query(Workflow).filter_by(id=1).one()
#     except NoResultFound:
#         print("NoResultFound")
#     except MultipleResultsFound:
#         print("MultipleResultsFound")
#     print(wfr)
#     print(wfr.wf_tasks)
#     print(wfr.first_wf_task)
#     print(Workflow.query.filter_by(id=1).one())
#     session
