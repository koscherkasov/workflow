from workflow.wf_state import WfConstants


class BaseTaskSpec(object):
    should_approve = False

    def start_execution_hook(self, task, wf_id, wf_engine):
        raise NotImplementedError()

    def approve_hook(self, wf_task, wf_id, wf_engine, **kwargs):
        if self.should_approve:
            raise NotImplementedError()

    def cancel_hook(self, wf_task, wf_id, wf_engine, **kwargs):
        if self.should_approve:
            raise NotImplementedError()

    def start_execution(self, task, wf_id, wf_engine):
        result_state = self.start_execution_hook(task, wf_id, wf_engine)
        if result_state:
            return result_state
        if self.should_approve:
            return WfConstants.WAITING
        return WfConstants.COMPLETED

    def update_children(self, task, wf_id, children_tasks_dict, wf_engine, factory):
        for task, task_spec in children_tasks_dict.items():
            task_spec.on_updated_hook(task, wf_id, wf_engine, factory)

    def on_updated_hook(self, task, wf_id, wf_engine, factory):
        if task.state == WfConstants.COMPLETED:
            task_spec = factory.get_task_spec(task)
            children_tasks_dict = {task: factory.get_task_spec(task) for task in task.children}
            task_spec.update_children(task, wf_id, children_tasks_dict, wf_engine, factory)
            return
        if task.state == WfConstants.WAITING:
            return
        task.set_state(WfConstants.READY)