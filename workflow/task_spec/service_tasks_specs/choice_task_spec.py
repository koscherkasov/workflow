from workflow.task_spec.base_task_spec import BaseTaskSpec


class ChoiceTaskSpec(BaseTaskSpec):
    should_approve = False

    def start_execution_hook(self, task, wf_id, wf_engine):
        print('CHOICE TASK execution {}, {}'.format(task, wf_id))

    def update_children(self, task, wf_id, children_tasks_dict, wf_engine, factory):
        raise NotImplementedError("Choice task {} hasn't implementation update_children method")
