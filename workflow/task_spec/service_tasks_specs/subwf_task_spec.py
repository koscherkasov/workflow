from workflow.task_spec.base_task_spec import BaseTaskSpec
from workflow.wf_state import WfConstants


class SubwfTaskSpec(BaseTaskSpec):
    should_approve = False

    def start_execution_hook(self, task, wf_id, wf_engine):
        print('SubwfTask TASK execution {}, {}'.format(task, wf_id))
        # clear all task state and start it.
        # It can be after cancelled task from this subwf and started to continue on parent wf
        subwf_id = task.subwf_id
        sub_wf = wf_engine.get_wf_by_id(subwf_id)
        for task in sub_wf.wf_tasks:
            if task.service_type == WfConstants.START:
                task.set_state(WfConstants.READY)
                continue
            task.set_state(WfConstants.FUTURE)
        result = wf_engine.start_wf(subwf_id)
        if result == WfConstants.COMPLETED:
            return WfConstants.COMPLETED
        return WfConstants.WAITING
