from workflow.task_spec.base_task_spec import BaseTaskSpec


class StartTaskSpec(BaseTaskSpec):
    should_approve = False

    def start_execution_hook(self, task, wf_id, wf_engine):
        print('START TASK execution {}, {}'.format(task, wf_id))
