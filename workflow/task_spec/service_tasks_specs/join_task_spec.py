from workflow.task_spec.base_task_spec import BaseTaskSpec
from workflow.wf_state import WfConstants


class JoinTaskSpec(BaseTaskSpec):
    should_approve = False

    def start_execution_hook(self, task, wf_id, wf_engine):
        print('JOIN TASK execution {}, {}'.format(task, wf_id))

    def on_updated_hook(self, task, wf_id, wf_engine, factory):
        for parent_task in task.parents:
            if parent_task.state != WfConstants.COMPLETED:
                task.set_state(WfConstants.WAITING)
                return
        task.set_state(WfConstants.READY)
