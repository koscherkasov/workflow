import datetime

from workflow.task_spec.service_tasks_specs.choice_task_spec import ChoiceTaskSpec


class ExampleChoiceTaskSpec(ChoiceTaskSpec):
    def update_children(self, task, wf_id, children_tasks_dict, wf_engine, factory):
        for task, task_spec in children_tasks_dict.items():
            if task.identification_name == 'B':
                task_spec.on_updated_hook(task, wf_id, wf_engine, factory)


class ExampleChoiceTaskSpec1(ChoiceTaskSpec):
    def update_children(self, task, wf_id, children_tasks_dict, wf_engine, factory):
        for task, task_spec in children_tasks_dict.items():
            if task.identification_name == 'C':
                task_spec.on_updated_hook(task, wf_id, wf_engine, factory)


class BigTestChoiceTaskSpec(ChoiceTaskSpec):
    def update_children(self, task, wf_id, children_tasks_dict, wf_engine, factory):
        now = datetime.datetime.now()
        result_way_dict = {0: 'N', 1: 'P', 2: 'Q'}
        result_number = now.hour % 3
        result_way = result_way_dict[result_number]
        for task, task_spec in children_tasks_dict.items():
            if task.identification_name == result_way:
                task_spec.on_updated_hook(task, wf_id, wf_engine, factory)
