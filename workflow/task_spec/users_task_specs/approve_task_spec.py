from workflow.task_spec.base_task_spec import BaseTaskSpec


class ApproveTaskSpec(BaseTaskSpec):
    should_approve = True

    def start_execution_hook(self, task, wf_id, wf_engine):
        print('execution {}, {}'.format(task, wf_id))

    def approve_hook(self, wf_task, wf_id, wf_engine, **kwargs):
        print('approve_hook {}, {}'.format(wf_task, wf_id))

    def cancel_hook(self, wf_task, wf_id, wf_engine, **kwargs):
        print('cancel_hook {}, {}'.format(wf_task, wf_id))


class NotApproveTaskSpec(BaseTaskSpec):
    should_approve = False

    def start_execution_hook(self, task, wf_id, wf_engine):
        print('execution {}, {}'.format(task, wf_id))
